
/*
fetch("https://jsonplaceholder.typicode.com/posts")
	.then((response) => response.json())
	.then((perResult) => showPosts(perResult));
*/

// querySelector("#id")
// querySelector("#id")
// querySelector("#id")
// querySelector("#id")

fetch("https://jsonplaceholder.typicode.com/posts")
	.then((response) => response.json())
	.then((result) => {
		console.log(result)
		return showPosts(result);
		//showPosts(result)
	}); 

const showPosts = (posts) => {

	let postEntries = ''

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}


document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	event.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 101
		}),

		headers: {'Content-type' : 'application/json ; charset=UTF-8'}
	})
	.then(response => response.json())
	.then(result => {
		console.log(result);
		alert('Successfully added.')
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	})

})



// EDIT POST

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
	document.querySelector('#btn-submit-update').removeAttribute('disabled');
}


// ------------ make UPDATE button functional


document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts/1", {
		//specify what HTTP method to be used
		method: 'PUT',
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		}),

		headers: {'Content-type' : 'application/json ; charset=UTF-8'}
	
	})
	.then((response) => response.json())
	.then(result => {
		console.log(result);
		alert('Successfully updated');

	// make the input boxes value null
	document.querySelector('#txt-edit-id').value = null;
	document.querySelector('#txt-edit-title').value = null;
	document.querySelector('#txt-edit-body').value = null;

	// re-disabled functionality
	document.querySelector('#btn-submit-update').setAttribute('disabled', true);


	})

});




// DELETE POST

// Have a fetch API code and specify what HTTP method to be used.

const deletePost = (id) => {
	document.querySelector(`#post-${id}`).remove();
	}
	
	fetch("https://jsonplaceholder.typicode.com/posts/${id}", {method: 'DELETE'});

	document.querySelector('#btn-submit-update').setAttribute('disabled', true);









